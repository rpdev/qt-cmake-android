# qt-cmake-android

The goal of this project is to provide means for building Qt C++ for Android applications both easily on the command line as well as from within Qt Creator - and this, without a lot of manual intervention.


## The Current Situation

At the time of writing this, the main tool to build Qt C++ project is `qmake`. It is very well possible to build Desktop applications using `cmake`, but for Android, building requires quite some digging into details. This will probably change, latest with Qt 6, as it seems that [`cmake` will be the default build system from that release on][qt-switch-to-cmake]. However, until this is the case, you still need to look for other solutions to build your Android Qt project using `cmake`.

The good news is that the guys over at KDAB looked into improving the support for `cmake` based projects right from within Qt Creator - you can check out [this blog post describing the changes they implemented][kdab-blog-post]. Even though at the time of writing this, the new Qt Creator release including these tweaks is not yet released, it will make running Android apps build with `cmake` from within Qt Creator much more straightforward.


## The Idea of This Project

The main intention of this project is the following: As the changes in Qt Creator mentioned above won't help us when we build our apps on the command line (which we still need, e.g. when we want to use CI/CD for building and delivering our apps), we still need to do some work in the `cmake` files of our projects. However, instead of having different interfaces when building our app inside Qt Creator vs on the command line, we want to rely on a single interface. In other words: We want to utilise the information Qt Creator is passing down to `cmake` when building in Qt Creator itself. When building on the command line, we will re-use the same information, passing it to `cmake` to ensure we have consistent builds.

To achieve this, this project provides a [tool chain file](https://cmake.org/cmake/help/latest/variable/CMAKE_TOOLCHAIN_FILE.html), which relies on the following information, that Qt Creator passes to `cmake`:

| **Variable** | **Type** | **Description** |
| ------------ | -------- | --------------- |
| **CMAKE_C_COMPILER** | `cmake` variable | The full path to the C compiler to be used. |
| **CMAKE_CXX_COMPILER** | `cmake` variable | The full path to the C++ compiler to be used. |
| **CMAKE_PREFIX_PATH** | `cmake` variable | The full path the Qt installation. This is where the Qt version specific `cmake` files can be found. |
| **QT_QMAKE_EXECUTABLE** | `cmake` variable | The full path to the `qmake` executable of the Qt installation which is used. |
| **ANDROID_NDK_ROOT** | Environment variable | The full path to the Android NDK used. |
| **ANDROID_SDK_ROOT** | Environment variable | The full path to the Android SDK used. |
| **ANDROID_NDK_PLATFORM** | Environment variable | The target Android version to compile for. |
| **ANDROID_NDK_HOST** | Environment variable | The host platform we are running on. |

**Note:** The Android SDK is required for packaging an app as APK. The `cmake` build itself usually doesn't require it, however, when using `androiddeployqt` from with `cmake`, the path to the SDK is needed.

**Note:** This project assumes a minimum Android NDK version r19c or higher. In particular, the project currently only allows building with the `clang` compiler, which - from this version on - is the default one.

**Note:** As you can see from the above, Qt Creator does not hand down the target architecture to build for. However, the target architecture is *built into* the Qt installation - each Qt installation is built for exactly one architecture, so we can *guess* the target from the path to the `qmake` executable. Hence, this project assumes the following folder layout (which matches the one created by the official Qt installer):

```
/path/to/Qt
└── 5.12.3
    ├── android_arm64_v8a
    ├── android_armv7
    └── android_x86
```

Where *5.12.3* would be the Qt version used and the sub-folders contain the individual Android ports of Qt. At the time of writing, there are no official `x86_64` ports of Qt, nevertheless, this projects supports this target. Just make sure the appropriate Qt is installed in a path that matches the pattern `/path/to/Qt/Version/android_x86_64`.


## Building on the Command Line

With the above, it now is really easy to build a `cmake` project on the command line:

```bash
export ANDROID_NDK_ROOT=/path/to/android-ndk
export ANDROID_SDK_ROOT=/path/to/android-sdk
export ANDROID_NDK_PLATFORM=android-21
mkdir build-android-arm
cd build-android-arm
cmake \
    -DCMAKE_TOOLCHAIN_FILE=/path/to/qt-cmake-android/cmake/toolchain-android.cmake \
    -DCMAKE_C_COMPILER=$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/bin/clang \
    -DCMAKE_CXX_COMPILER=$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/bin/clang++ \
    -DCMAKE_PREFIX_PATH=/path/to/Qt/5.12.3/android_armv7 \
    -DQT_QMAKE_EXECUTABLE=/path/to/Qt/5.12.3/android_armv7/bin/qmake \
    ..
make
```

To build for another Android target architecture, just modify the path in the `QT_QMAKE_EXECUTABLE` and make sure that `CMAKE_PREFIX_PATH` contains the path to the right Qt installation as well. That's it.


## Building From Inside Qt Creator

As the project uses information that Qt Creator anyway passes down to `cmake`, there is not much to do. However, if you check the instructions for building on the command line, you might have noticed that we set the path to the tool chain file manually when invoking `cmake`. In theory, we could do the very same in Qt Creator, however, this requires manual work when opening the project and setting up compilation for another Android target.

To simplify this even more, this project provides a utility script called `autodetect-android-build.cmake`, which allows us to skip passing down the tool chain file manually. Ideally, you include a copy of this project within your own project. When using `git`, you can add it as a submodule:

```bash
mkdir 3rdparty
git submodule add \
    https://gitlab.com/rpdev/qt-cmake-android.git 3rdparty/ \
    qt-cmake-android
```

Next, add the following to your top-level `CMakeLists.txt` file:

```cmake
# Auto-detect Android build from within Qt Creator:
include(./3rdparty/qt-cmake-android/cmake/autodetect-android-build.cmake)
```

That's it! You should now be able to just open this project in Qt Creator and build for any Qt for Android flavor your want.


## How to Actually Build an APK?

With the steps described above, we can now build our project for Android. However, we still need to package the app as an APK. The good thing is, this project also provides the means for doing so.

In particular, it also re-uses the very same interface that the (future) Qt Creator will use and which is described in the [KDAB blog post](kdab-blog-post). Packaging is controlled by the following `cmake` variables:

| **Variable** | **Description** |
| ------------ | --------------- |
| **ANDROID_PACKAGE_SOURCE_DIR** | The path to directory containing additional files for the Android APK build. This directory should contain e.g. your app's `AndroidManifest.xml` file. |
| **ANDROID_EXTRA_LIBS** | A list of additional libraries that need to be deployed with your app. The most common example might be the OpenSSL libraries (in case you need secure network connections), however, other libraries might need to be added as well in that list. |

**Note:** While it is not required by this project, the variables mentioned above should be set as cache variables. Otherwise, deploying and running your APK later on from within Qt Creator might not work.

Another thing we need to consider is, that on Android, the entry point for an app is in Java, from where control is passed on to the C++ side later on. Consequentially, we need to build our Android *application* as a *shared library* instead. Long story short, a `CMakeLists.txt` file of a possible application might look as simple as this:

```cmake
set(SOURCES main.cpp)

find_package(Qt5Qml REQUIRED)
find_package(Qt5Quick REQUIRED)

if(ANDROID)
    add_library(
        MyApp 
        SHARED 
        ${SOURCES}
    )
    set(
        ANDROID_PACKAGE_SOURCE_DIR 
        ${CMAKE_CURRENT_SOURCE_DIR}/android
        CACHE INTERNAL "Extra files for Android deployment"
    )
    set(
        ANDROID_EXTRA_LIBS 
        ${CMAKE_CURRENT_SOURCE_DIR}/prebuild/android-${ANDROID_ABI}/libcrypto.so
        ${CMAKE_CURRENT_SOURCE_DIR}/prebuild/android-${ANDROID_ABI}/libssl.so
        CACHE INTERNAL "Extra libraries to include in APK"
    )
    qt_android_build_apk(
        TARGET MyApp
        PACKAGE_NAME com.example.myapp
        QML_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR}
    )
else()
    add_executable(
        MyApp
        ${SOURCES}
    )
endif()

target_link_libraries(MyApp Qt5::Qml Qt5::Quick)

```

Notice the following:

1. We check if we are building for Android or not. Depending on this, we either use `add_executable` or `add_library`.
2. In the Android case, we set `ANDROID_PACKAGE_SOURCE_DIR` and `ANDROID_EXTRA_LIBS`. Both are actually optional, however, you most probably want to set `ANDROID_PACKAGE_SOURCE_DIR`. Both libraries are stored in the cache (`CACHE INTERNAL`), so Qt Creator will later on be able to see these variables.
3. We use the function `qt_android_build_apk` provided by this project to package our app as APK. The function need to know the target to package, the package name as well as the path to where the QML files (if applicable) of the app are stored.

The last point results in a new target to be created, which we can use to package our app:

```bash
make MyApp-apk
```

In addition, another target is created, which can be used to deploy to the primary Android device connected to the build host:

```bash
make MyApp-apk-install
```


## Reporting Problems

If you encounter any issues with this project, feel free to report a bug in the project's [bug tracker][bug-tracker]. Before reporting, please make sure you carefully read this document, as this project has several requirements towards your build environment, in particular w.r.t. the folder structure used as well as how information is passed down to the build system.


[qt-switch-to-cmake]: https://blog.qt.io/blog/2018/10/29/deprecation-of-qbs/
[kdab-blog-post]: https://www.kdab.com/qtcreator-cmake-for-android-plugin/
[bug-tracker]: https://gitlab.com/rpdev/qt-cmake-android/issues