###############################################################################
# ANDROID TOOLCHAIN FILE FOR BUILDING QT APPS
#
# This is a "wrapper" toolchain file, which can be used to build Qt apps
# both from within Qt Creator as well as on the command line.
# For this, it mainly relies on the variables that Qt Creator passes down
# to cmake when configuring. In particular, these are:
#
# - CMAKE_C_COMPILER: Points to the Android C compiler.
# - CMAKE_CXX_COMPILER: Points to the Android C++ compiler.
# - CMAKE_PREFIX_PATH: Path to Qt installation.
# - QT_QMAKE_EXECUTABLE: Path to the qmake executable of the Qt installation.
#
# In addition, it will use the following environment variables (if present),
# which also are set within Qt Creator:
#
# - ANDROID_NDK_ROOT: Points to the Android NDK folder.
# - ANDROID_SDK_ROOT: Points to the Android SDK folder.
# - ANDROID_NDK_PLATFORM: The Android target platform (i.e. version).
# - ANDROID_NDK_HOST: The host platform we run on.
#
# This toolchain will use these variables to "guess" the correct locations
# of e.g. the Android NDK. For this, it assumes the following:
#
# - Use of a fully blown Android NDK, r19c or higher.
# - Use of clang as compiler.
# - Use of the standard Qt installation as installed via the Qt maintenance
#   tool or at least an installation where the architecture specific
#   folder paths match what the Qt installer would set up.
# - The Android SDK is located in the same folder as the Android NDK used.
###############################################################################


###############################################################################
# Utility Functions
###############################################################################

# Check if DIR is an Android SDK folder
function(qt_cmake_android_check_is_sdk DIR VAR)
    if(IS_DIRECTORY ${DIR}/build-tools AND
            IS_DIRECTORY ${DIR}/platform-tools AND
            IS_DIRECTORY ${DIR}/platforms AND
            IS_DIRECTORY ${DIR}/emulator)
        set(${VAR} ON PARENT_SCOPE)
    else()
        set(${VAR} OFF PARENT_SCOPE)
    endif()
endfunction()


###############################################################################
# Android NDK Detection
###############################################################################

# Check if the user explicitly set a path to the Android NDK:
if(ANDROID_NDK_ROOT)
    # In this case, there is nothing to do...

# Check of the ANDROID_NDK_ROOT environment variable is set. It is usually
# set by Qt Creator, so this is the way to go to get it if it is not explicitly
# set:
elseif(NOT "$ENV{ANDROID_NDK_ROOT}" STREQUAL "")
    set(ANDROID_NDK_ROOT "$ENV{ANDROID_NDK_ROOT}")

# If we still did not find the NDK root, derive it from the compiler.
# This assumes we are using an NDK version greater or equal to r19c and we
# are using the clang compiler:
elseif(CMAKE_CXX_COMPILER)
    get_filename_component(ANDROID_NDK_ROOT ${CMAKE_CXX_COMPILER} DIRECTORY)
    get_filename_component(ANDROID_NDK_ROOT ${ANDROID_NDK_ROOT} DIRECTORY)
    get_filename_component(ANDROID_NDK_ROOT ${ANDROID_NDK_ROOT} DIRECTORY)
    get_filename_component(ANDROID_NDK_ROOT ${ANDROID_NDK_ROOT} DIRECTORY)
    get_filename_component(ANDROID_NDK_ROOT ${ANDROID_NDK_ROOT} DIRECTORY)
    get_filename_component(ANDROID_NDK_ROOT ${ANDROID_NDK_ROOT} DIRECTORY)
endif()

if(ANDROID_NDK_ROOT)
    set(ANDROID_NDK_ROOT "${ANDROID_NDK_ROOT}" CACHE PATH
        "The Android NDK root folder")
    message("Using Android NDK in ${ANDROID_NDK_ROOT}")
else()
    message(FATAL_ERROR "Could not find ANDROID_NDK_ROOT")
    return()
endif()


###############################################################################
# Android SDK Detection
###############################################################################

# Check if the user explicitly set the path to the Android SDK:
if(ANDROID_SDK_ROOT)
    # Nothing else to do...

# Next, check if the ANDROID_SDK_ROOT environment variable is set. Qt Creator
# sets it, so this is the next best thing to check:
elseif(NOT "$ENV{ANDROID_SDK_ROOT}" STREQUAL "")
    set(ANDROID_SDK_ROOT "$ENV{ANDROID_SDK_ROOT}")

# If we still did not find the SDK, we apply a heuristic. We assume, that
# the SDK is installed in a folder next to the Android NDK. Hence, we check
# every sibling folder of the NDK one if it looks like an SDK one:
else()
    get_filename_component(dir ${ANDROID_NDK_ROOT} DIRECTORY)
    file(GLOB dirs ${dir}/*)
    foreach(dir ${dirs})
        qt_cmake_android_check_is_sdk("${dir}" is_sdk)
        if(is_sdk)
            set(ANDROID_SDK_ROOT ${dir})
            break()
        endif()
    endforeach()
    unset(dirs)
    unset(dir)
    unset(is_sdk)
endif()

if(ANDROID_SDK_ROOT)
    set(ANDROID_SDK_ROOT "${ANDROID_SDK_ROOT}" CACHE PATH
        "The Android SDK root folder")
    message(STATUS "Using Android SDK in ${ANDROID_SDK_ROOT}")
else()
    message(WARNING "No Android SDK root found")
endif()


###############################################################################
# Set Up CMake Search Paths
###############################################################################

# Set CMAKE_FIND_ROOT_PATH to Qt installation prefix
if(QT_QMAKE_EXECUTABLE)
    get_filename_component(_QT_DIR ${QT_QMAKE_EXECUTABLE} DIRECTORY)
    get_filename_component(_QT_DIR ${_QT_DIR} DIRECTORY)
    list(APPEND CMAKE_FIND_ROOT_PATH ${_QT_DIR})
    unset(_QT_DIR)
endif()


###############################################################################
# Auto-Detect Target Properties
###############################################################################

# Guess defaults to use
set(DEFAULT_ANDROID_TOOLCHAIN clang) # Use clang
set(DEFAULT_ANDROID_API 19) # Target Android 21 API
set(DEFAULT_ANDROID_STL c++_shared) # Use shared C++ STL
set(DEFAULT_ANDROID_ABI armeabi-v7a) # By default, target 32 bit ARM

# Refine defaults by checking against the Qt version we use:
if(QT_QMAKE_EXECUTABLE)
    if(QT_QMAKE_EXECUTABLE MATCHES "/+android_armv7/+bin/+qmake")
        message(STATUS "Detected Android arm build")
        set(DEFAULT_ANDROID_ABI armeabi-v7a)
        set(DEFAULT_ANDROID_ARCH arm)
    elseif(QT_QMAKE_EXECUTABLE MATCHES "/+android_arm64_v8a/+bin/+qmake")
        message(STATUS "Detected Android arm64 build")
        set(DEFAULT_ANDROID_ABI arm64-v8a)
        set(DEFAULT_ANDROID_ARCH arm64)
        set(DEFAULT_ANDROID_LINKER_FLAGS "-fuse-ld=gold")
        set(DEFAULT_ANDROID_API 21)
    elseif(QT_QMAKE_EXECUTABLE MATCHES "/+android_x86_64/+bin/+qmake")
        message(STATUS "Detected Android x86_64 build")
        set(DEFAULT_ANDROID_ABI x86_64)
        set(DEFAULT_ANDROID_ARCH x86_64)
        set(DEFAULT_ANDROID_API 21)
    elseif(QT_QMAKE_EXECUTABLE MATCHES "/+android_x86/+bin/+qmake")
        message(STATUS "Detected Android x86 build")
        set(DEFAULT_ANDROID_ABI x86)
        set(DEFAULT_ANDROID_ARCH x86)
    else()
        message(
            WARNING "Cannot guess ANDROID_ABI from ${QT_QMAKE_EXECUTABLE}"
        )
    endif()
endif()

# Apply defaults, unless the user explicitly set some:
set(ANDROID_STL "${DEFAULT_ANDROID_STL}" CACHE INTERNAL "")
set(ANDROID_TOOLCHAIN clang CACHE INTERNAL "")
set(ANDROID_PLATFORM "android-${DEFAULT_ANDROID_API}" CACHE INTERNAL "")
set(ANDROID_ABI "${DEFAULT_ANDROID_ABI}" CACHE INTERNAL "")

# Note: the following could be used once cmake support for NDK r19c and
#       up is fixed (see https://gitlab.kitware.com/cmake/cmake/issues/18739).
#       Until then, we rely on the NDK's built in tool chain file, which
#       requires other variables to be set.
#set(CMAKE_SYSTEM_NAME Android)
#if(NOT CMAKE_SYSTEM_VERSION)
#    set(CMAKE_SYSTEM_VERSION "${DEFAULT_ANDROID_API}")
#endif()
#if(NOT CMAKE_ANDROID_NDK)
#    set(CMAKE_ANDROID_NDK "${ANDROID_NDK_ROOT}")
#endif()
#if(NOT CMAKE_ANDROID_NDK_TOOLCHAIN_VERSION)
#    # Use clang
#    set(CMAKE_ANDROID_NDK_TOOLCHAIN_VERSION clang)
#endif()
#if(NOT CMAKE_ANDROID_ARCH)
#    set(CMAKE_ANDROID_ARCH "${DEFAULT_ANDROID_ARCH}")
#endif()
#if(NOT CMAKE_ANDROID_NDK_TOOLCHAIN_VERSION)
#    set(CMAKE_ANDROID_NDK_TOOLCHAIN_VERSION clang)
#endif()
#if(NOT CMAKE_ANDROID_ARCH_ABI)
#    set(CMAKE_ANDROID_ARCH_ABI "${DEFAULT_ANDROID_ABI}")
#endif()
#if(NOT CMAKE_ANDROID_API)
#    set(CMAKE_ANDROID_API "${DEFAULT_ANDROID_API}")
#endif()
#if(NOT CMAKE_ANDROID_STL_TYPE)
#    set(CMAKE_ANDROID_STL_TYPE "${DEFAULT_ANDROID_STL}")
#endif()


###############################################################################
# Hand Over to NDK Toolchain File
###############################################################################

message(STATUS "Including Android NDK Toolchain File")
include("${ANDROID_NDK_ROOT}/build/cmake/android.toolchain.cmake")


###############################################################################
# Hand Over to CMake
###############################################################################

# Note: This currently does not work, see
#       https://gitlab.kitware.com/cmake/cmake/issues/18739 for more
#       information.
#include(${CMAKE_ROOT}/Modules/Platform/Android-Clang.cmake REQUIRED)
#include(${CMAKE_ROOT}/Modules/Platform/Android-Initialize.cmake REQUIRED)


###############################################################################
# Some Final Tweaks...
###############################################################################

# Apply some additional linker flags if required:
foreach(var CMAKE_SHARED_LINKER_FLAGS CMAKE_EXE_LINKER_FLAGS)
    # Use gold linker, see
    # https://github.com/android-ndk/ndk/issues/148:
    set(${var} "${${var}} ${DEFAULT_ANDROID_LINKER_FLAGS}")
endforeach()

# Include utility functions:
include(${CMAKE_CURRENT_LIST_DIR}/apk-utils.cmake)
