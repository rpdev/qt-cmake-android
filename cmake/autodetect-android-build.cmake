# If a toolchain file is explicitly set, skip auto-detection:
if(CMAKE_TOOLCHAIN_FILE)
    return()
endif()

if(
        QT_QMAKE_EXECUTABLE
        MATCHES
        "/+android_(armv7|arm64_v8a|x86|x86_64)/+bin/+qmake(.exe)?$"
        )
    message(STATUS "Detected Android build")
    include(${CMAKE_CURRENT_LIST_DIR}/toolchain-android.cmake)
endif()
